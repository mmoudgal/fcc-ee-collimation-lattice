!--------------------------------------------------------------
! Example FCC-ee MAD-X script
! This script prepares the Z (E=45.6 GeV) lattice for ensuing tracking studies.
! The thick sequences is loaded.
! Depending on the setting, the lattice will be tapered and a thin version for tracking will be exported.
!--------------------------------------------------------------

SET, FORMAT="19.15f";
option,update_from_parent=true; // new option in mad-x as of 2/2019

!--------------------------------------------------------------
! General settings
!--------------------------------------------------------------

RadiationAndRF = 1; // 1 to enable RF, radiation, and tapering; 0 to disable them
OffmomentumOptics = 1; // 1 to create offmomentum twiss

!--------------------------------------------------------------
! Lattice selection and beam parameters
!--------------------------------------------------------------

CALL, FILE="../sequences/MAD-X/FCCee_z_217_nosol_20.seq";

pbeam :=   45.6;
EXbeam = 0.27e-9;
EYbeam = 1.0e-12;
Nbun :=    16640;
NPar :=   1.7e11;


Ebeam := sqrt( pbeam^2 + emass^2 );

// Beam defined without radiation as a start - radiation is turned on later depending on the requirements
BEAM, PARTICLE=POSITRON, NPART=Npar, KBUNCH=Nbun, ENERGY=Ebeam, RADIATE=FALSE, BV=+1, EX=EXbeam, EY=EYbeam;

!--------------------------------------------------------------
! Load aperture definitions and perform sequence edits
!--------------------------------------------------------------

// Load the aperture definition
CALL, FILE="../sequences/MAD-X/aperture/FCCee_aper_definitions.madx";
CALL, FILE="../sequences/MAD-X/aperture/install_synchrotron_rad_masks.madx";


!-------------------------------------------------------------------------------
! Perform initial TWISS and survey in an ideal machine without radiation
!-------------------------------------------------------------------------------

USE, SEQUENCE = L000013;

// Save the voltage settings for the cavities for later use if needed
VOLTCA1SAVE = VOLTCA1; 


SHOW, VOLTCA1SAVE;

// Turn off the cavities for ideal machine twiss and survey
VOLTCA1 = 0;


SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K0L,K1L,K2L,K3L,K4L,K1SL,K2SL,K3SL,K4SL,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE;

// Place where the initial conditions are saved - used for RF matching later if needed
SAVEBETA, LABEL=B.IP, PLACE=#s, SEQUENCE=L000013;

SURVEY, file="survey_madx_z_b1.tfs";
TWISS, FILE = "twiss_z_b1_nottapered.tfs", TOLERANCE=1E-12; ! Twiss without radiation and tapering
APERTURE, HALO={6,6,6,6}, FILE="aperture_z_b1_nottapered.tfs";

!-------------------------------------------------------------------------------
! Create offmomentum twiss
!-------------------------------------------------------------------------------

if (OffmomentumOptics==1){   
  ! using maximum stable dp/p
  TWISS, SEQUENCE=L000013, FILE='twiss_z_b1_-dp.tfs', DELTAP=-0.014;
  TWISS, SEQUENCE=L000013, FILE='twiss_z_b1_+dp.tfs', DELTAP=0.013;
  
}
stop;

!-------------------------------------------------------------------------------
! Perform RF matching and tapering if radiaiton is on
!-------------------------------------------------------------------------------

if (RadiationAndRF==1){   // RF and tapering are enabled

  // RF back on
  VOLTCA1 = VOLTCA1SAVE;
  

  // Turn the beam radiation on. N.B. This simple toggle works only if the sequence is not defined in the orginal beam command.
  BEAM, RADIATE=TRUE;

  // RF matching
  LAGCA1 = 0.5;
  

  MATCH, sequence=L000013, BETA0 = B.IP, tapering;
    VARY, NAME=LAGCA1, step=1.0E-7;
    CONSTRAINT, SEQUENCE=L000013, RANGE=#e, PT=0.0;
    JACOBIAN,TOLERANCE=1.0E-14, CALLS=3000;
  ENDMATCH;

  // Twiss with tapering
  USE, SEQUENCE = L000013;

  SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,KS,KSL,K0L,K1L,K2L,K3L,K4L,K1S,K2S,K3S,K4S,HKICK,VKICK,BETX,BETY,ALFX,ALFY,MUX,MUY,DX,DY,DPX,DPY,R11,R12,R22,R21,X,PX,Y,PY,T,PT,DELTAP,VOLT,LAG,HARMON,FREQ,E1,E2,APERTYPE,APER_1,APER_2,APER_3,APER_4,TILT,ANGLE,ASSEMBLY_ID,MECH_SEP;

  TWISS, TAPERING, FILE="twiss_z_b1_tapered.tfs", TOLERANCE= 1E-12;
}


!-------------------------------------------------------------------------------
! Slice the lattice and save a thin sequence
!-------------------------------------------------------------------------------
// Note: if the tapering was enabled in the previous steps, MAKETHIN will inherit the
// tapered magnetic strenghts (KNTAP) calculated by TWISS and the element strengths (KN) accordingly to produce a tapered thin sequence.
// If any of: RF, beam radiation, tapering; were disabled before, the resulting thin sequence is not tapered.

// Slicing with special attention ot IR quads and sextupoles     
SELECT, FLAG=makethin, CLASS=RFCAVITY, SLICE = 1;
SELECT, FLAG=makethin, CLASS=rbend, SLICE = 4;
SELECT, FLAG=makethin, CLASS=quadrupole, SLICE = 4;
SELECT, FLAG=makethin, CLASS=sextupole, SLICE = 4;
        
SELECT, FLAG=makethin, PATTERN="QC*", SLICE=20;
SELECT, FLAG=makethin, PATTERN="SY*", SLICE=20;
        
MAKETHIN, SEQUENCE=L000013, STYLE=TEAPOT, MAKEDIPEDGE=false;

USE, SEQUENCE = L000013;
        
// Save the thin sequence
if (RadiationAndRF==1){
   SAVE, SEQUENCE=L000013, FILE="tapered_z_b1_thin.seq", BEAM=True;
   TWISS, FILE="twiss_z_b1_thin_tapered.tfs", TOLERANCE=1E-12;
}
else{
   SAVE, SEQUENCE=L000013, FILE="nottapered_z_b1_thin.seq", BEAM=True;
   TWISS, FILE="twiss_z_b1_thin_nottapered.tfs", TOLERANCE=1E-12;
}

/* // SixTrack export
USE, SEQUENCE = L000013;
// Reference radius chosen as 1/3 of inter-pole distance for the warm FCC-ee dipoles of 84 mm.
SIXTRACK, APERTURE=TRUE, CAVALL=TRUE, radius=14E-03;
*/

/* // Tracking
USE, SEQUENCE = L000013;
       
TRACK, ONETABLE=true, FILE="no_tapering_track.dat", DAMP=FALSE, QUANTUM=FALSE;

CALL, FILE="madx_inrays.dat";

RUN, TURNS=1;
ENDTRACK;
*/
