from pathlib import Path
import pytest
import json
import pandas as pd
from pandas._testing import assert_frame_equal, assert_series_equal
import tfs
import numpy as np

TWISS_DIR = Path(__file__).parent.parent/'scripts'

REFERENCE_FILE = json.load(open(Path(__file__).parent/"reference_file.json"))

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

PLANES = ['X', 'Y', 'Z']

def test_emit(operation_mode):
    twiss=tfs.read(TWISS_DIR/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_X']) < TUNE_ACCURACY
    assert abs(twiss.headers['EY'] - REFERENCE_FILE[operation_mode]['EMITTANCE_Y']) < TUNE_ACCURACY
    assert abs(twiss.headers['PC'] - REFERENCE_FILE[operation_mode]['ENERGY']) < TUNE_ACCURACY


def test_tune(operation_mode):
    twiss=tfs.read(TWISS_DIR/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    assert abs(twiss.headers['Q1']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_betastar(operation_mode):
    twiss=tfs.read(TWISS_DIR/f"twiss_{operation_mode}_b1_nottapered.tfs", index="NAME")
    for ip in ['IP.1','IP.2','IP.3','IP.4']:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY
 

def test_symmetry(operation_mode):
    # load twiss and remove collimators and drifts, as those lead to an asymmetry in the dataframe indices
    twiss=tfs.read(TWISS_DIR/f"twiss_{operation_mode}_b1_nottapered.tfs", index='NAME')
    mask = twiss.index.str.contains('TCP|TCS|DRIFT_', regex=True)
    twiss=twiss[~mask]
    
    # assume two fold symmetry
    # compare on optics functions, but drop names as those are different left to right
    optics_parameters=('BETX', 'BETY', 'ALFX', 'ALFY', 'DX')
    assert_frame_equal(twiss.loc['IP.1':'IP.2', optics_parameters].reset_index().drop(labels=['NAME'], axis=1), 
                       twiss.loc['IP.3':'IP.4', optics_parameters].reset_index().drop(labels=['NAME'], axis=1),
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01)


def test_aperture(operation_mode):
    aperture=tfs.read(TWISS_DIR/f"aperture_{operation_mode}_b1_nottapered.tfs", index='NAME')
    
    assert aperture.headers['N1MIN'] > min(REFERENCE_FILE[operation_mode]['PRIMARY_COLLIMATOR_APERTURE_X'],
                                           REFERENCE_FILE[operation_mode]['PRIMARY_COLLIMATOR_APERTURE_Y'])
    # assert 'TCP' in aperture.headers['AT_ELEMENT']
    mask = aperture.index.str.contains('TCP|TCS', regex=True)
    aperture=aperture[~mask]
    assert all(aperture['N1']>REFERENCE_FILE[operation_mode]['APERTURE_BOTTLENECK']['N1'])


def test_survey(operation_mode):
    survey=tfs.read(TWISS_DIR/f"survey_madx_{operation_mode}_b1.tfs")

    # check that ring is closed by comparing X,Y,Z of start and endpoint
    assert_series_equal(survey[PLANES].iloc[0],
                       survey[PLANES].iloc[-1],
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01, check_names=False) 
    assert abs(survey['THETA'].abs().iloc[-1] - 2*np.pi) < BETA_ACCURACY

    # add interpol1d for s vs r/sqrt(x**2+y**2) and compare to Geo survey
    
